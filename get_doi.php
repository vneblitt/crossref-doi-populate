<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Get Crossref XML from Any DOI</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />
    </head>
    <body>

        <h1>Get Crossref Metadata by DOI</h1>
        
        <p><a href="three_parses.html">Back</a></p>

        <form action="get_metadata.php" method="post">
            <p><strong>DOI</strong>: <input type="text" name="doi" size="200">
            <input type="submit" name="submit" value="Get Metadata">
            </p>
        </form>
        
        <h2>DOIs to Try</h2>
        <ul>
            <li>10.1149/06917.0455ecst</li>
            <li>10.1063/1.4919059</li>
            <li>10.1016/j.apenergy.2015.03.017</li>
            <li>10.1145/2676870.2676887</li>
            <li>10.1145/2685553.2702671</li>
            <li>10.1615/IHTC15.cpm.009342</li>
            <li>10.1117/12.2084464</li>
            <li>10.1117/12.2216566</li>
            <li>10.1117/12.2083030</li>
            <li>10.1117/12.2082192</li>
        </ul>
    <div class="footer">
        <script type="text/javascript" src="js/footer.js"></script>
    </div>
    </body>
</html>