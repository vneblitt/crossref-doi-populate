<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Parsing Journal Article Crossref XML</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />
    </head>
    <body>
        <h1>Journal Article</h1>

<?php
// This is for journal articles

// header('content-type: text/plain');
//$file = 'xml-article/catalyst-layer-ionomer-imaging-of-fuel-cells.xml';
//$file = 'xml-article/new-strain-states.xml';
$file = 'xml-article/optimization-problems.xml';

$XMLreaderDoc = new XMLReader();
$XMLreaderDoc->open($file, 'utf-8', LIBXML_NOBLANKS);

$XMLreaderDoc->next();

while ($XMLreaderDoc->read()) {
    if ($XMLreaderDoc->name == 'full_title' && $XMLreaderDoc->readOuterXml() != '<full_title/>') { 
        $journaltitle = $XMLreaderDoc->readInnerXml();
        echo '<strong>Journal Title</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->nodeType === XMLREADER::ELEMENT && $XMLreaderDoc->localName === 'issn') {
                $issn_type = $XMLreaderDoc->getAttribute('media_type');
                
                if ($issn_type == 'print') {
                        $issn = $XMLreaderDoc->readInnerXml();
                        echo '<strong>ISSN</strong>: ' . $issn . '<br>';
                } elseif ($issn_type == 'electronic') {
                        $eissn = $XMLreaderDoc->readInnerXml();
                        echo '<strong>EISSN</strong>: ' . $eissn . '<br>';
                } else {
                        echo '<strong>ISSN</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
                }
    }
    if ($XMLreaderDoc->name == 'volume' && $XMLreaderDoc->readOuterXml() != '<volume/>') {
        $volume = $XMLreaderDoc->readInnerXml();    
        echo '<strong>Volume</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'issue' && $XMLreaderDoc->readOuterXml() != '<issue/>') {
        $issue = $XMLreaderDoc->readInnerXml(); 
        echo '<strong>Issue</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'title' && $XMLreaderDoc->readOuterXml() != '<title/>') {
        $articletitle = $XMLreaderDoc->readInnerXml(); 
        echo '<strong>Article Title</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'first_page' && $XMLreaderDoc->readOuterXml() != '<first_page/>') {
        $firstpage = $XMLreaderDoc->readInnerXml();
        echo '<strong>First Page/Article No.</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'last_page' && $XMLreaderDoc->readOuterXml() != '<last_page/>') {
        $lastpage = $XMLreaderDoc->readInnerXml();
        echo '<strong>Last Page</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->nodeType === XMLREADER::ELEMENT && $XMLreaderDoc->localName === 'item_number') {
        $item_number_type = $XMLreaderDoc->getAttribute('item_number_type');
        
        if ($item_number_type == 'article_number') {
        echo '<strong>Article Number</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
        }
    }
    if ($XMLreaderDoc->name == 'doi' && $XMLreaderDoc->readOuterXml() != '<doi/>') {
        $doi = $XMLreaderDoc->readInnerXml();
        echo '<strong>DOI</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
        // echo '<strong>DOI</strong>: <a href="http://dx.doi.org/' . $doi . '">' . $doi . '</a><br>';
    }
    if ($XMLreaderDoc->name == 'month' && $XMLreaderDoc->readOuterXml() != '<month/>') {
        $month = $XMLreaderDoc->readInnerXml();
        $month = sprintf('%02d', $month);
        echo '<strong>Publication Month</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
        if ($XMLreaderDoc->name == 'day' && $XMLreaderDoc->readOuterXml() != '<day/>') {
        $day = $XMLreaderDoc->readInnerXml();
        $day = sprintf('%02d', $day);
        echo '<strong>Publication Month</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'year' && $XMLreaderDoc->readOuterXml() != '<year/>') {
        $year = $XMLreaderDoc->readInnerXml();
        echo '<strong>Publication Year</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
}



if (isset($day)) {
    $pubdate = $year . '-' . $month . '-' . $day;
} else {
    $pubdate = $year . '-' . $month;
}

$XMLreaderDoc->close();

echo '<p><a href="three_parses.html">Back</a></p>';

?>

    <p>The information from the XML needs to auto-populate into this form:</p>
    <form>
        <p><strong>Article Title: </strong><input type="text" name="jarticle_title" value="<?php echo $articletitle ?>" size="100"/></p>
        <p><strong>Journal Title: </strong><input type="text" name="journal_title" value="<?php echo $journaltitle ?>" size="100"/></p>
        <p>
            <strong>Volume: </strong><input type="text" name="jvolume" value="<?php echo $volume ?>" />
            <strong>Issue: </strong><input type="text" name="jissue" value="<?php echo $issue ?>" />
            <strong>First Page/Article No.: </strong><input type="text" name="jfpage" value="<?php echo $firstpage ?>" />
            <strong>Last Page: </strong><input type="text" name="jlpage" value="<?php echo $lastpage ?>" />
        </p>
        <p>
            <strong>Pub Date: </strong><input type="text" name="jpubdate" value="<?php echo $pubdate ?>"/>
            <strong>First Online Date: </strong><input type="text" name="jonlinedate"/>
        </p>
        <p>
            <strong>ISSN: </strong><input type="text" name="jissn" value="<?php echo $issn ?>"/>
            <strong>EISSN: </strong><input type="text" name="jeissn" value="<?php echo $eissn ?>"/>
        </p>
        
    </form>
    
    <div class="footer">
        <script type="text/javascript" src="js/footer.js"></script>
    </div>

    </body>
</html>
