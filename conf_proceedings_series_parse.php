<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Parsing Conference Proceedings in a Series Article Crossref XML</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />
    </head>
    <body>
        <h1>Conference Proceedings in a Series Article</h1>

<?php
// This is for conference proceedings in a series articles

// header('content-type: text/plain');
$file = 'xml-conf-series/a-haptic-inspired-audio-approach-for-structural-health-monitoring-decision-making.xml';
//$file = 'xml-conf-series/frequency-space-prediction-filtering-for-acoustic-clutter-and-random-noise-attenuation-in-ultrasound-imaging.xml';
//$file = 'xml-conf-series/remote-readable-graphite-oxide.xml';
//$file = 'xml-conf-series/ultrasound-bent-ray-tomography-with-a-modified-total-variation-regularization-scheme.xml';

$XMLreaderDoc = new XMLReader();
$XMLreaderDoc->open($file, 'utf-8', LIBXML_NOBLANKS);

$XMLreaderDoc->next();

while ($XMLreaderDoc->read()) {
    
    if ($XMLreaderDoc->name == 'conference_name' && $XMLreaderDoc->readOuterXml() != '<conference_name/>') { 
        $conname = $XMLreaderDoc->readInnerXml();
        echo '<strong>Conference Name</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->name == 'conference_location' && $XMLreaderDoc->readOuterXml() != '<conference_location/>') { 
        $conlocation = $XMLreaderDoc->readInnerXml();
        echo '<strong>Conference Location</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->name == 'conference_date' && $XMLreaderDoc->readOuterXml() != '<conference_date/>') { 
        echo '<strong>Conference Date</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    //series title
    if ($XMLreaderDoc->name == 'title' && $XMLreaderDoc->readOuterXml() != '<title/>') {
        $series_title = $XMLreaderDoc->readInnerXml();
        echo '<strong>Series Title</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->name == 'issn' && $XMLreaderDoc->readOuterXml() != '<issn/>') {
        echo '<strong>ISSN</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'proceedings_title' && $XMLreaderDoc->readOuterXml() != '<proceedings_title/>') { 
        $proceedingstitle = $XMLreaderDoc->readInnerXml();
        echo '<strong>Proceedings Title</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->name == 'isbn' && $XMLreaderDoc->readOuterXml() != '<isbn/>') {
        echo '<strong>ISBN</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'volume' && $XMLreaderDoc->readOuterXml() != '<volume/>') {
        $volume = $XMLreaderDoc->readInnerXml();
        echo '<strong>Volume</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'issue' && $XMLreaderDoc->readOuterXml() != '<issue/>') {
        $issue = $XMLreaderDoc->readInnerXml();
        echo '<strong>Issue</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
        // article title
    if ($XMLreaderDoc->name == 'title' && $XMLreaderDoc->readOuterXml() != '<title/>') {
        $article_title = $XMLreaderDoc->readInnerXml();
        echo '<strong>Article Title</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->name == 'first_page' && $XMLreaderDoc->readOuterXml() != '<first_page/>') {
        $firstpage = $XMLreaderDoc->readInnerXml();
        echo '<strong>First Page/Article No.</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'last_page' && $XMLreaderDoc->readOuterXml() != '<last_page/>') {
        $lastpage = $XMLreaderDoc->readInnerXml();
        echo '<strong>Last Page</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'doi' && $XMLreaderDoc->readOuterXml() != '<doi/>') {
        echo '<strong>DOI</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'month' && $XMLreaderDoc->readOuterXml() != '<month/>') {
        $month = $XMLreaderDoc->readInnerXml();
        $month = sprintf('%02d', $month);
        echo '<strong>Publication Month</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'day' && $XMLreaderDoc->readOuterXml() != '<day/>') {
        $day = $XMLreaderDoc->readInnerXml();
        $day = sprintf('%02d', $day);
        echo '<strong>Publication Day</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    if ($XMLreaderDoc->name == 'year' && $XMLreaderDoc->readOuterXml() != '<year/>') {
        $year = $XMLreaderDoc->readInnerXml();
        echo '<strong>Publication Year</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
}

if (isset($day)) {
    $pubdate = $year . '-' . $month . '-' . $day;
} else {
    $pubdate = $year . '-' . $month;
}

$XMLreaderDoc->close();

echo '<p><a href="three_parses.html">Back</a></p>';

?>

<p>The information from the XML needs to auto-populate into this form:</p>
    <form>
        <p><strong>Article Title: </strong><input type="text" name="csarticle_title" value="<?php echo $article_title ?>"size="100"/></p>
        <div class="fieldset">Conference Information</div>
        <p><strong>Conference Name: </strong><input type="text" name="csconference_name" value="<?php echo $conname ?>" size="100"/><br></p>
        <p>
            <strong>Conference Location: </strong><input type="text" name="csconference_location" value="<?php echo $conlocation ?>" size="50" />
            <strong>Conference Date(s): </strong><input type="text" name="csconference_dates"/>
        </p>
        <div class="fieldset">Series Information</div>
        <p><strong>Series Title: </strong><input type="text" name="cstitle" value="<?php echo $series_title ?>" size="100"/></p>
        <p>
            <strong>ISSN: </strong><input type="text" name="csisbn"/>
            <strong>EISSN: </strong><input type="text" name="cseisbn"/>
        </p>
        <div class="fieldset">Proceedings Information</div>
        <p><strong>Proceedings Title: </strong><input type="text" name="csproceedings_title" value="<?php echo $proceedingstitle ?>"size="100"/></p>
        <p>
            <strong>Volume: </strong><input type="text" name="csvolume" value="<?php echo $volume ?>"/>
            <strong>Pub Date: </strong><input type="text" name="cspubdate"/>
            <strong>First Online Date: </strong><input type="text" name="csonlinedate"/>
        </p>
        
        <div class="fieldset">Article Information</div>
        <p>
            <strong>Pub Date: </strong><input type="text" name="csapubdate" value="<?php echo $pubdate ?>"/>
            <strong>First Online Date: </strong><input type="text" name="csaonlinedate"/>
        </p>
        <p>
            <strong>First Page/Article No.: </strong><input type="text" name="csfpage"/ value="<?php echo $firstpage ?>">
            <strong>Last Page: </strong><input type="text" name="cslpage" value="<?php echo $lastpage ?>"/>
        </p>


        
    </form>

    <div class="footer">
        <script type="text/javascript" src="js/footer.js"></script>
    </div>

    </body>
</html>