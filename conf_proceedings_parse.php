<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Parsing Conference Proceedings Article Crossref XML</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />
    </head>
    <body>
        <h1>Conference Proceedings Article</h1>

<?php
// This is for conference proceedings articles

// header('content-type: text/plain');
//$file = 'xml-conf/asymmetric-memory-extension-for-openshmem.xml';
//$file = 'xml-conf/harnessing-twitter-and-crowdsourcing-to-augment-aurora-forecasting.xml';
$file = 'xml-conf/hp-adaptive.xml';

$XMLreaderDoc = new XMLReader();
$XMLreaderDoc->open($file, 'utf-8', LIBXML_NOBLANKS);

$XMLreaderDoc->next();

while ($XMLreaderDoc->read()) {
    
    if ($XMLreaderDoc->name == 'conference_name' && $XMLreaderDoc->readOuterXml() != '<conference_name/>') { 
        $conname = $XMLreaderDoc->readInnerXml();
        echo '<strong>Conference Name</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    
    if ($XMLreaderDoc->name == 'conference_location' && $XMLreaderDoc->readOuterXml() != '<conference_location/>') { 
        $conlocation = $XMLreaderDoc->readInnerXml();
        echo '<strong>Conference Location</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    
    if ($XMLreaderDoc->name == 'conference_date' && $XMLreaderDoc->readOuterXml() != '<conference_date/>') { 
        echo '<strong>Conference Date</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    
    if ($XMLreaderDoc->name == 'proceedings_title' && $XMLreaderDoc->readOuterXml() != '<proceedings_title/>') { 
        $proceedingstitle = $XMLreaderDoc->readInnerXml();
        echo '<strong>Proceedings Title</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    
    if ($XMLreaderDoc->name == 'isbn' && $XMLreaderDoc->readOuterXml() != '<isbn/>') {
        $isbn = $XMLreaderDoc->readInnerXml();
        echo '<strong>ISBN</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'volume' && $XMLreaderDoc->readOuterXml() != '<volume/>') {
        echo '<strong>Volume</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'issue' && $XMLreaderDoc->readOuterXml() != '<issue/>') {
        echo '<strong>Issue</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'title' && $XMLreaderDoc->readOuterXml() != '<title/>') {
        $articletitle = $XMLreaderDoc->readInnerXml();
        echo '<strong>Article Title</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'first_page' && $XMLreaderDoc->readOuterXml() != '<first_page/>') {
        $firstpage = $XMLreaderDoc->readInnerXml();
        echo '<strong>First Page/Article No.</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'last_page' && $XMLreaderDoc->readOuterXml() != '<last_page/>') {
        $lastpage = $XMLreaderDoc->readInnerXml();
        echo '<strong>Last Page</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
   
    /*if ($XMLreaderDoc->name == 'doi' && $XMLreaderDoc->readOuterXml() != '<doi/>') {
        echo '<strong>DOI</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }*/
    
    if ($XMLreaderDoc->name == 'doi' && $XMLreaderDoc->readOuterXml() != '<doi/>') {
        $doi = $XMLreaderDoc->readInnerXml();
        echo '<strong>DOI</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
        // echo '<strong>DOI</strong>: <a href="http://dx.doi.org/' . $doi . '">' . $doi . '</a><br>';
    }
    
    if ($XMLreaderDoc->name == 'month' && $XMLreaderDoc->readOuterXml() != '<month/>') {
        $month = $XMLreaderDoc->readInnerXml();
        $month = sprintf('%02d', $month);
        echo '<strong>Publication Month</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'day' && $XMLreaderDoc->readOuterXml() != '<day/>') {
        $day = $XMLreaderDoc->readInnerXml();
        $day = sprintf('%02d', $day);
        echo '<strong>Publication Day</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
    
    if ($XMLreaderDoc->name == 'year' && $XMLreaderDoc->readOuterXml() != '<year/>') {
        $year = $XMLreaderDoc->readInnerXml();
        $year = sprintf('%02d', $year);
        echo '<strong>Publication Year</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    }
}

if (isset($day)) {
    $pubdate = $year . '-' . $month . '-' . $day;
} elseif (isset($month) && isset($year)) {
    $pubdate = $year . '-' . $month;
} else {
    $pubdate = $year;
}

$XMLreaderDoc->close();

echo '<p><a href="three_parses.html">Back</a></p>';

?>

    <p>The information from the XML needs to auto-populate into this form:</p>
    <form>
        <p><strong>Article Title: </strong><input type="text" name="carticle_title" value="<?php echo $articletitle ?>" size="100"/></p>
        <div class="fieldset">Conference Information</div>
        <p><strong>Conference Name: </strong><input type="text" name="cconference_name" value="<?php echo $conname ?>" size="100"/><br></p>
        <p>
            <strong>Conference Location: </strong><input type="text" name="cconference_location" value="<?php echo $conlocation ?>" size="50"/>
            <strong>Conference Date(s): </strong><input type="text" name="cconference_dates"/>
        </p>
        <div class="fieldset">Proceedings Information</div>
        <p><strong>Proceedings Title: </strong><input type="text" name="cproceedings_title" value="<?php echo $proceedingstitle ?>" size="100"/></p>
        <p>
            <strong>Pub Date: </strong><input type="text" name="cpubdate"/>
            <strong>First Online Date: </strong><input type="text" name="conlinedate"/>
        </p>
        <p>
            <strong>ISBN: </strong><input type="text" name="cisbn" value="<?php echo $isbn ?>"/>
            <strong>EISBN: </strong><input type="text" name="ceisbn"/>
        </p>
        <div class="fieldset">Article Information</div>
        <p>
            <strong>Pub Date: </strong><input type="text" name="capubdate" value="<?php echo $pubdate ?>"/>
            <strong>First Online Date: </strong><input type="text" name="caonlinedate"/>
        </p>
        <p>
            <strong>First Page/Article No.: </strong><input type="text" name="cfpage" value="<?php echo $firstpage ?>" />
            <strong>Last Page: </strong><input type="text" name="clpage" value="<?php echo $lastpage ?>" />
        </p>


        
    </form>
    
    <div class="footer">
        <script type="text/javascript" src="js/footer.js"></script>
    </div>

    </body>
</html>

