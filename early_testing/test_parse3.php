<?php
// This is for journal articles
echo '*************************<br>';
echo 'This is a journal article<br>';
echo '*************************<br>';

// header('content-type: text/plain');
$file = 'new-strain-states.xml';
$XMLreaderDoc = new XMLReader();
$XMLreaderDoc->open($file, 'utf-8', LIBXML_NOBLANKS);

$XMLreaderDoc->next();

while ($XMLreaderDoc->read()) {
    if ($XMLreaderDoc->name == 'full_title' && $XMLreaderDoc->readOuterXml() != '<full_title/>') { 
        echo '<strong>Journal Title</strong>: ' . $XMLreaderDoc->readInnerXml() . '<br>';
    }
    if ($XMLreaderDoc->name == 'issn' && $XMLreaderDoc->readOuterXml() != '<issn/>')
        echo '<strong>ISSN</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'volume' && $XMLreaderDoc->readOuterXml() != '<volume/>')
        echo '<strong>Volume</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'issue' && $XMLreaderDoc->readOuterXml() != '<issue/>')
        echo '<strong>Issue</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'title' && $XMLreaderDoc->readOuterXml() != '<title/>')
        echo '<strong>Article Title</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'first_page' && $XMLreaderDoc->readOuterXml() != '<first_page/>')
        echo '<strong>First Page/Article No.</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'last_page' && $XMLreaderDoc->readOuterXml() != '<last_page/>')
        echo '<strong>Last Page</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'item_number' && $XMLreaderDoc->readOuterXml() != '<item_number/>')
        echo '<strong>Item Number</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'doi' && $XMLreaderDoc->readOuterXml() != '<doi/>')
        echo '<strong>DOI</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'month' && $XMLreaderDoc->readOuterXml() != '<month/>')
        echo '<strong>Publication Month</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
    if ($XMLreaderDoc->name == 'year' && $XMLreaderDoc->readOuterXml() != '<year/>')
        echo '<strong>Publication Year</strong>: ' . $XMLreaderDoc->readInnerXml(). '<br>';
}

$XMLreaderDoc->close();

// Issues to fix:
// 1. Tags with attributes results in the output of a second tag with blank content
// 2. Does not ignore blank spaces after end of beginning tag and before the first character of content
// 3. How to distinguish non-unique tags